﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class RegistrationPage : System.Web.UI.Page
{

    public string ConnString = "Data Source=PETROV-PC\\SQLEXPRESS;Integrated Security=True";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            SqlConnection conn = new SqlConnection(ConnString);
            SqlCommand cmd = new SqlCommand("Select count(*) from UsersTable where Name= @Name", conn);
            cmd.Parameters.AddWithValue("@Name", this.TextBox1.Text);
            conn.Open();
            var result = cmd.ExecuteScalar();
            if (result != null)
            {
                Response.Write("<script>alert('User Created!');</script>");
             
            }
            else
            {
                Response.Write("<script>alert('User already exists!');</script>");

            }


        }
    }
    
   
    protected void Button1_Click(object sender, EventArgs e)
    {
        Guid idGuid = Guid.NewGuid();

        SqlConnection conn = new SqlConnection(ConnString);
        conn.Open();
        if (conn.State == System.Data.ConnectionState.Open)
        {

            SqlCommand cmd = new SqlCommand("INSERT INTO UsersTable (id, Name, Email, Password) VALUES(@id ,@Name, @Email , @Password)", conn);
          
            cmd.Parameters.AddWithValue("id", idGuid.ToString());
            cmd.Parameters.AddWithValue("Name", TextBox1.Text);
            cmd.Parameters.AddWithValue("Email", TextBox2.Text);
            cmd.Parameters.AddWithValue("Password", TextBox3.Text);
            cmd.ExecuteNonQuery();
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";

            

        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/LogIn.aspx");
    }
}