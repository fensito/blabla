﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogIn.aspx.cs" Inherits="LogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>

<body>
    <form id="form1" runat="server">
        <div><h1>Log In</h1></div>
    <div>
        <table>
            <tr>
                <td>
                     <asp:Label ID="lblUserName" runat="server" Text="User Name :"></asp:Label>
                     

                     <asp:TextBox ID="tbUserName" runat="server" ></asp:TextBox>

                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbUserName" runat="server" ErrorMessage="PLease Insert Username"></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="lblPassword" runat="server" Text="Password :"></asp:Label>
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" ></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tbPassword" runat="server" Display="Dynamic" ErrorMessage="Please Insert Password"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="lblIfRegistered" runat="server" Text="If you are not registered click "></asp:Label>
                    <asp:HyperLink id="hpRedirectToRegPage" runat="server" Text="Here" NavigateUrl="~/RegistrationPage.aspx"></asp:HyperLink>
                   
                </td>
            </tr>
            <tr>
                <td style="text-align:right;">
                    <asp:Button runat="server" ID="btnLogin" Text="Log In" OnClick="btnLogin_Click" />
                </td>
            </tr>
        </table>
   
       
    </div>
    </form>
</body>
</html>
