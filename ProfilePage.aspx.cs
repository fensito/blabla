﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class ProfilePage : System.Web.UI.Page
{
    public string ConnString = "Data Source=PETROV-PC\\SQLEXPRESS;Integrated Security=True";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["New"] != null)
        {
            form1.Visible = true;


            SqlConnection conn = new SqlConnection(ConnString);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Select count(*) from UsersTable where ", conn);
            lbluserId.Text = Session["New"].ToString();
            lblPrices.Text = Price.Instance.GetPrice().ToString();







        }
        else
        {
            Response.Redirect("~/LogIn.aspx");


        }
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {

        Session["New"] = null;
        Response.Redirect("~/LogIn.aspx");
    }




    public class Price
    {
        private Random rnd = new Random();
        private static Price instance;
        private decimal price = 1;
        private Price()
        { }

        public decimal GetPrice()
        {

            int CurrentHour = DateTime.Now.Hour;
            if (CurrentHour != PreviousHour)
            {

                double Random = rnd.NextDouble() * 2.0 ;
                 price = price * (decimal)Random;
                PreviousHour = CurrentHour;
            }
            return price;
        }

        private int PreviousHour = -1;
        public static Price Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Price();
                }
                return instance;
            }
        }
    }
}